var http = require('http')
let date_ob = new Date();
http
    .createServer(function (req, res) {
        res.writeHead(200, { 'Content-Type': 'text/plain' })
        res.end(`Tatchai Wijitwiengrat\n ${date_ob}`)
    })
    .listen(2337, '127.0.0.1')
console.log('Server running at http://127.0.0.1:2337/')
